'''
Created on 2020年3月31日

@author: Jiashu Wang
'''
from ubidots import ApiClient
from labs.common.SensorData import SensorData
from labs.module08.MqttClientConnector import MqttClientConnector
import logging
from labs.common.ActuatorDataListener import ActuatorDataListener
class UbidotsClientConnector(object):
    '''
    classdocs
    '''

    def __init__(self):
        '''
        Constructor
        '''
    def publishActuatorDataUsingMqtt(self, ActuatorData):   
        mqtt_client = MqttClientConnector()
        mqtt_client.publishActuatorCommand(ActuatorData, 2) 
        logging.info("Publish Actuator Data using MQTT")

    def publishSensorDataUsingMqtt(self, SensorData):
        mqtt_client = MqttClientConnector()
        mqtt_client.publishSensorData(SensorData, 2)
       
        logging.info("Publish Sensor Data using MQTT")
    def publishSensorDataUsingAPI(self, SensorData):
        api = ApiClient(token='BBFF-fxFo9o01O7DpeoHvn07pRyOFQo6T8D')
        my_variable = api.get_variable('5e8294891d847261570adb37')
        new_value = my_variable.save_value({'value': SensorData.curValue})
        logging.info("Publish Sensor Data using API")
        
    def subscribeToTopic(self, qos):
        mqtt_client = MqttClientConnector()
        mqtt_client.subscribeActuatorData( qos)
        logging.info("Subscribe to Actuator Data")