'''
Created on 2020年4月12日

@author: Jiashu Wang
'''
from projectGateway.RemoteServiceConnector import RemoteServiceConnector
from labs.common.ActuatorData import ActuatorData
from projectGateway import client_sub

# subscribe to the Ubidots variable and send actuator data to device
def sendDataToDevice():
    remoteService = RemoteServiceConnector()

    remoteService.getActuatorData()    
    remoteService.publishActuatorDatatoDevice()
# subscribe to device 
def getDataFromDevice():
    client_sub.subscribeData()
    client_sub.subscribeCamera()
if __name__ == '__main__':
#     while True:
    getDataFromDevice()
    sendDataToDevice()
    pass
