'''
Created on 2020年3月1日

@author: Jiashu Wang
'''
  # Ensures paho is in PYTHONPATH
import paho.mqtt.client as mqtt
from time import sleep
import logging
from labs.common.SensorData import SensorData
from labs.common.DataUtil import DataUtil
from labs.module08.UbidotsClientConnector import UbidotsClientConnector
msgPayload = None
logging.basicConfig(level = logging.INFO, format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)
def on_connect(mqttc, obj, flags, rc):
    print("rc: " + str(rc))

# callback function, get the message from device and check it is 
# humidity data or temperature data and send the data to Ubidots
def on_message_sensor(mqttc, obj, msg):
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))
#     logging.info(msg.payload)
    global msgPayload
    msgPayload = msg.payload
    dataUtil = DataUtil()
    sensorData = dataUtil.toSensorDataFromJson(msgPayload)
    ubidotsClientConnector = UbidotsClientConnector()
    if sensorData.getName() == 'APIHumidity':
        ubidotsClientConnector.publishHumiDataUsingAPI(sensorData)
    elif sensorData.getName() == 'Temperature':
        ubidotsClientConnector.publishTempDataUsingAPI(sensorData)
    sleep(5)    
# callback function, get the camera message from device send the data to Ubidots
def on_message_camera(mqttc, obj, msg):
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))
#     logging.info(msg.payload)
    UbidotsClientConnector.publishCameraCommandUsingAPI(self, msg.payload)
    sleep(5)    
    
def on_publish(mqttc, obj, mid):
    print("publish: " + str(mid))


def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))


def on_log(mqttc, obj, level, string):
    print(string)

def on_disconnect(client, userdata, rc):
    if rc != 0:
        print("Unexpected disconnection.")
# If you want to use a specific client id, use
# mqttc = mqtt.Client("client-id")
# but note that the client id must be unique on the broker. Leaving the client
# id parameter empty will generate a random id for you.
def subscribeData():
    mqttc = mqtt.Client()
    mqttc.on_message = on_message_sensor
    mqttc.on_connect = on_connect
    mqttc.on_disconnect = on_disconnect
    mqttc.on_publish = on_publish
    mqttc.on_subscribe = on_subscribe
# Uncomment to enable debug messages
# mqttc.on_log = on_log
    mqttc.connect("127.0.0.1", 1883, 60)
    mqttc.loop_start()
    
    mqttc.subscribe("SensorData", 2)
    sleep(5)
#mqttc.disconnect()
  #  mqttc.loop_stop()
#     mqttc.loop_forever(timeout=5)
    while True:
        mqttc.loop()
        sleep(5)

#subscribe to the camera
def subscribeCamera():
    mqttc = mqtt.Client()
    mqttc.on_message = on_message_camera
    mqttc.on_connect = on_connect
    mqttc.on_disconnect = on_disconnect
    mqttc.on_publish = on_publish
    mqttc.on_subscribe = on_subscribe
# Uncomment to enable debug messages
# mqttc.on_log = on_log
    mqttc.connect("127.0.0.1", 1883, 60)
    mqttc.loop_start()
    
    mqttc.subscribe("SensorData", 2)
    sleep(5)
#mqttc.disconnect()
  #  mqttc.loop_stop()
#     mqttc.loop_forever(timeout=5)
    while True:
        mqttc.loop()
        sleep(5)


