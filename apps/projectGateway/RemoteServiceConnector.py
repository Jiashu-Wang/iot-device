'''
Created on 2020年4月13日

@author: Jiashu Wang
'''
from projectGateway import HttpSubscribe
from labs.common.ActuatorData import ActuatorData
from projectGateway import client_pub
from labs.common.DataUtil import DataUtil
from labs.common.SensorData import SensorData

class RemoteServiceConnector(object):
    '''
    classdocs
    '''
    
    actuatorData = ActuatorData()
    dataUtil = DataUtil()
    sensorData = SensorData()
    def __init__(self, params):
        '''
        Constructor
        '''
        
        
    def getActuatorData(self):
        actuatorCommand = HttpSubscribe.get_var()
        self.actuatorData.addValue(actuatorCommand)
        return self.actuatorData
    def publishActuatorDatatoDevice(self):
        jsonData = self.dataUtil.toJsonFromActuatorData(self.actuatorData)
        client_pub.publishActuatorData(jsonData)
    
#     def getSensorData(self):
        