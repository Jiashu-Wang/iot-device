'''
Created on 2020年4月13日

@author: Jiashu Wang
'''
from project import client_pub, client_sub
class RemoteServiceManager(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
 # publish data to gateway       
    def sendDataToGateway(self, jsonData):
        client_pub.publishSensorData(jsonData)
        return True
 # subscribe to actuator data   
    def subscribeToGateway(self, actuatorData):
        client_sub.subscribeData()