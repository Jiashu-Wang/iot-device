'''
Created on 2020年3月1日

@author: Jiashu Wang
'''
  # Ensures paho is in PYTHONPATH
import paho.mqtt.client as mqtt
from time import sleep
import logging
from labs.common.SensorData import SensorData
from labs.common.DataUtil import DataUtil
msgPayload = None
logging.basicConfig(level = logging.INFO, format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)
def on_connect(mqttc, obj, flags, rc):
    print("rc: " + str(rc))


def on_message(mqttc, obj, msg):
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))
    logging.info(msg.payload)
    global msgPayload
    msgPayload = msg.payload
    
    
def on_publish(mqttc, obj, mid):
    print("publish: " + str(mid))


def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))


def on_log(mqttc, obj, level, string):
    print(string)

def on_disconnect(client, userdata, rc):
    if rc != 0:
        print("Unexpected disconnection.")
# If you want to use a specific client id, use
# mqttc = mqtt.Client("client-id")
# but note that the client id must be unique on the broker. Leaving the client
# id parameter empty will generate a random id for you.
def subscribeData():
    mqttc = mqtt.Client()
    mqttc.on_message = on_message
    mqttc.on_connect = on_connect
    mqttc.on_disconnect = on_disconnect
    mqttc.on_publish = on_publish
    mqttc.on_subscribe = on_subscribe
# Uncomment to enable debug messages
# mqttc.on_log = on_log
    mqttc.connect("192.168.0.107", 1883, 60)
    mqttc.loop_start()
    
    mqttc.subscribe("SensorData", 1)
    
    sleep(5)
#mqttc.disconnect()
  #  mqttc.loop_stop()
    mqttc.loop_forever()

