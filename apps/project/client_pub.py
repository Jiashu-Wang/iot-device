'''
Created on 2020年3月1日

@author: Jiashu Wang
'''
  # Ensures paho is in PYTHONPATH
import paho.mqtt.client as mqtt
from labs.common.SensorData import SensorData
from labs.common.DataUtil import DataUtil
import logging

logging.basicConfig(level = logging.INFO, format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

def on_connect(mqttc, obj, flags, rc):
    print("rc: " + str(rc))


def on_message(mqttc, obj, msg):
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))


def on_publish(mqttc, obj, mid):
    print("mid: " + str(mid))
    pass


def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))


def on_log(mqttc, obj, level, string):
    print(string)


# If you want to use a specific client id, use
# mqttc = mqtt.Client("client-id")
# but note that the client id must be unique on the broker. Leaving the client
# id parameter empty will generate a random id for you.
senserData = SensorData()
dataUtil = DataUtil()
senserData.addValue(20)
senserData.setName('Humidity')
jsonData = dataUtil.toJsonFromSensorData(senserData)


def publishSensorData(jsonData):
    mqttc = mqtt.Client()
    mqttc.on_message = on_message
    mqttc.on_connect = on_connect
    mqttc.on_publish = on_publish
    mqttc.on_subscribe = on_subscribe
# Uncomment to enable debug messages
# mqttc.on_log = on_log
    mqttc.connect("127.0.0.1", 1883, 60)
    
    mqttc.loop_start()

# print("tuple")
# (rc, mid) = mqttc.publish("tuple", "bar", qos=2)
    print("SensorData")
    infot = mqttc.publish("SensorData", jsonData, qos=2)
    
    infot.wait_for_publish()

def publishCamCommand(jsonData):
    mqttc = mqtt.Client()
    mqttc.on_message = on_message
    mqttc.on_connect = on_connect
    mqttc.on_publish = on_publish
    mqttc.on_subscribe = on_subscribe
# Uncomment to enable debug messages
# mqttc.on_log = on_log
    mqttc.connect("127.0.0.1", 1883, 60)
    
    mqttc.loop_start()

# print("tuple")
# (rc, mid) = mqttc.publish("tuple", "bar", qos=2)
    print("CameraCommand")
    infot = mqttc.publish("SensorData", jsonData, qos=2)
    
    infot.wait_for_publish()
#mqttc.disconnect()