'''
Created on 2020年4月13日

@author: Jiashu Wang
'''
from labs.module01.SystemPerformanceAdaptor import SystemPerformanceAdaptor
from time import sleep
from labs.common.SensorData import SensorData
from labs.common.DataUtil import DataUtil
from labs.module03.SensorDataManager import SensorDataManager
from labs.module04.HumiditySensorAdaptorTask import HumiditySensorAdaptorTask
from project.RemoteServiceManager import RemoteServiceManager
import logging
from project import piCamDetect
tempData = SensorData()
humiData = SensorData()
dataUtil = DataUtil()
remoteService = RemoteServiceManager()

#log the CPU and memory utilization 
def logSystemUtil():
    
    SystemPerformanceAdaptor.showPerformance()
    
# set up temperature sensor function    
def handleTempData():
    tempSensor = SensorDataManager()
    tempSensor.getSensorData()
    
    tempData = tempSensor.sensorData
    print("\ntempData")
    print(tempData)
    
    jsonTempData = dataUtil.toJsonFromSensorData(tempData)
    remoteService.sendDataToGateway(jsonTempData)
    return jsonTempData

# set up humidity sensor function 
def handleHumidityData():
    humiSensor = HumiditySensorAdaptorTask()
    humiSensor.start()
    humiData = humiSensor.sensorData
    jsonHumiData = dataUtil.toJsonFromSensorData(humiData)
    remoteService.sendDataToGateway(jsonHumiData)
    return jsonHumiData




if __name__ == '__main__':
    while True:
        logSystemUtil()
        
        handleTempData()
        handleHumidityData()
        piCamDetect.runCamera()
        sleep(30)
    pass