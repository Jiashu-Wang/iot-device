'''
Created on 2020年2月15日

@author: Jiashu Wang
'''
from labs.module05.HumiditySensorAdaptorTask import HumiditySensorAdaptorTask
import logging
from labs.common.PersistenceUtil import PersistenceUtil
class MultiSensorAdaptor(object):
    '''
    classdocs
    '''
    sensordata = None
    

    def __init__(self):
        '''
        Constructor
        '''
    
    '''
    start the thread to get data from senseHat,
    calculate the error range between API and I2C humidity data
    '''
    def updateSensorData(self):
        hs = HumiditySensorAdaptorTask()

        hs.start()
        
        
        