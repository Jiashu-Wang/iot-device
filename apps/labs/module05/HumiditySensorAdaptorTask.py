'''
Created on 2020年2月15日

@author: Jiashu Wang
'''
from labs.common.SensorData import SensorData

from sense_hat import SenseHat
from time import sleep
import threading
import logging
from labs.common.PersistenceUtil import PersistenceUtil
import labs.module05.SensorDataManager
class HumiditySensorAdaptorTask(threading.Thread):
    '''
    To get humidity data from senseHat API
    '''
    sensorData = SensorData()
    currentHum = 0
    maxVal = 30
    minVal = 0
    intervalTime = 10
    senseHat = SenseHat

    def __init__(self):
        '''
        Constructor
        '''
        threading.Thread.__init__(self)
    
    def getCurrentHum(self):
        return self.currentHum    
    def run(self):
        while True:
            self.senseHat = SenseHat()
            # use get_humidity to read data
            self.currentHum = self.senseHat.get_humidity()
            self.sensorData.addValue(self.currentHum)
            self.sensorData.setName("APIHumidity")
            
            logging.info("Humidity from  API" + str(self.currentHum))
            print("write sensor data to redis")
            json = PersistenceUtil.writeSensorDataToDbms(self, self.sensorData)
            logging.info("Generate json: " + str(json))
            
            SDM = labs.module05.SensorDataManager.SensorDataManager()
            SDM.handleSensorData(self.sensorData)
            
            
            
            
            sleep(self.intervalTime)
            