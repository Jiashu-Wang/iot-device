'''
Created on 2020年3月14日

@author: Jiashu Wang
'''
from coapthon.client.helperclient import HelperClient
from labs.common.SensorData import SensorData
from labs.common.DataUtil import DataUtil
import logging

host = '127.0.0.1'
port = 5683
path = "temp"
# generate sensor data
senserData = SensorData()
dataUtil = DataUtil()
senserData.addValue(20)
senserData.setName('Humidity')
jsonData = dataUtil.toJsonFromSensorData(senserData)
#create a coap client
client = HelperClient(server=(host, port))
client.put(path, jsonData)
logging.info("put path")

response = client.get(path)
client.post(path, jsonData)
logging.info("get path")
client.delete(path)
logging.info("delete path")
print("response: " + str(response))
client.stop()