'''
Created on 2020年2月19日

@author: Jiashu Wang
'''
import json
from labs.common.SensorData import SensorData
from labs.common.ActuatorData import ActuatorData
import logging
class DataUtil(object):
    '''
    classdocs
    '''
    senseData = SensorData
    actuator = ActuatorData
    

    def __init__(self):
        '''
        Constructor
        '''
        
    def toJsonFromSensorData(self, SensorData):
        data = {
#            'SensorData':{
                'name': SensorData.name,
                'timeStamp': SensorData.timeStamp,
                'curValue': SensorData.curValue,
                'avgValue': SensorData.avgValue,
                'minValue': SensorData.minValue,
                'maxValue': SensorData.maxValue,
                'sampleCount': SensorData.sampleCount,
#                },
            }
        
        sdjson = json.dumps(data)
        logging.info(sdjson)
        return sdjson
        
    def toSensorDataFromJson(self, jsonData):
        sdDict = json.loads(jsonData)
        
        sd = SensorData()
        sd.name = sdDict['name']
        sd.timeStamp = sdDict['timeStamp']
        sd.curValue = sdDict['curValue']
        sd.avgValue = sdDict['avgValue']
        sd.minValue = sdDict['minValue']
        sd.maxValue = sdDict['maxValue']
        sd.sampleCount = sdDict['sampleCount']
        
        return sd
        
    def writeSensorDataToFile(self):
        return 0
        
    def toJsonFromActuatorData(self, ActuatorData):
        data = {
#            'ActuatorData':{
                'name': ActuatorData.name,
                'timeStamp': ActuatorData.timeStamp,
                'command': ActuatorData.command,
                'statusCode': ActuatorData.statusCode,
                'newVal': ActuatorData.newVal,
 #               },
            }
        
        adjson = json.dumps(data)
        logging.info(adjson)
        return adjson
    
    def toActuatorDataFromJson(self, jsonData):
        adDict = json.loads(jsonData)
        
        ad = ActuatorData()
        ad.name = adDict['name']
        ad.timeStamp = adDict['timeStamp']
        ad.command = adDict['command']
        ad.statusCode = adDict['statusCode']
        ad.newVal = adDict['newVal']
        
        return ad
    def writeActuatorDataToFile(self):
        return 0