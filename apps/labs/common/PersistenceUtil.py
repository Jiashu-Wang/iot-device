'''
Created on 2020年2月19日

@author: Jiashu Wang
'''
from labs.common.ActuatorDataListener import ActuatorDataListener
from labs.common.SensorDataListener import SensorDataListener
from labs.common.ActuatorData import ActuatorData
from labs.common.SensorData import SensorData
from labs.common.DataUtil import DataUtil
from redis import StrictRedis
import logging
class PersistenceUtil(object):
    '''
    classdocs
    '''
    actuatorListener = ActuatorDataListener()
    sensorListener = SensorDataListener()


    def __init__(self):
        '''
        Constructor
        '''
    def registerActuatorDbmsListener(self,onMessage):
        onMessage()
        
    def registerSensorDbmsListener(self,onMessage):
        
        redis = StrictRedis(host = 'localhost', port = 6379 , db = 0,)
        ps = redis.pubsub()
        ps.subscribe('Sensor')
        for item in ps.listen():
            if item['type'] == 'message':
                logging.info(item['data'])
        ps.close()        

    '''
    write actuator data to redis and publish a message
    '''
    def writeActuatorDataToDbms(self, ActuatorData):
        redis = StrictRedis(host = 'localhost', port = 6379 , db = 0,)
        
        jsonActuatorData = DataUtil.toJsonFromActuatorData(self, ActuatorData)
        redis.set('ActuatorData', jsonActuatorData)
        redis.publish('Actuator', 'Actuator Data Update.')
        logging.info(redis.get('ActuatorData'))        
        redis.close()
#        PersistenceUtil.registerActuatorDbmsListener(PersistenceUtil.actuatorListener.onActuatorMessage(ActuatorData))       
        return jsonActuatorData
    '''
    write sensor data to redis and publish a message
    '''    
    def writeSensorDataToDbms(self, SensorData):
        redis = StrictRedis(host = 'localhost', port = 6379 , db = 0,)
        
        jsonSensorData = DataUtil.toJsonFromSensorData(self, SensorData)
        redis.set('SensorData', jsonSensorData)
        redis.publish('Sensor', 'Sensor Data Update.')
        logging.info("SensorData from Redis: " + str(redis.get('SensorData')))
        redis.close()
#        PersistenceUtil.registerSensorDbmsListener(PersistenceUtil.sensorListener.onSensorMessage(SensorData))
        return jsonSensorData