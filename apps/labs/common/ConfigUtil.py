'''
Created on Jan 29, 2020

@author: Jiashu Wang
'''

'''
the ConfigUtil class provides methods to load the config file, 
check whether the file contains config data
and get the value by section name and key name  
'''

import os
import configparser

defaultPath ='D:/NEU/6530/workspace/iot-device/config/ConnectedDevicesConfig.props'

class ConfigUtil(object):
    '''
    classdocs
    '''
    configFile = defaultPath;
    configValue = configparser.ConfigParser()

    def __init__(self, filePath):
        '''
        Constructor
        '''
        self.loadConfig(filePath)
        
    
    '''
    The first parameter is ‘section’, which represents the config file section to parse. The
    second parameter is ‘key’, which represents the key of the value within the given section to return.
    '''
    def getValue(self, section, key):        
        return self.configValue.get(section, key)
    '''
    The first parameter is ‘section’, which represents the config file section to parse. The
    second parameter is ‘key’, which represents the key of the value within the given section to return.
    '''
    def hasConfigData(self):
        if self.configValue.sections() != None:
            return True
        return False
    '''
    The first and only parameter is ‘fileName’, which represents the relative or absolute file
    name to load. If null or empty, the default will be used.On success, ‘true’ is returned; otherwise, ‘false’ is returned.
    '''
    
    def loadConfig(self, fileName):
        if fileName != None:
            self.configFile = fileName
        if os.path.exists(self.configFile):
            self.configValue.read(self.configFile)
            return True
        return False
    

            
        