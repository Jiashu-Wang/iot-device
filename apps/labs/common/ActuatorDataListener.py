'''
Created on 2020年2月19日

@author: Jiashu Wang
'''
from labs.common.ActuatorData import ActuatorData
from redis import StrictRedis
import logging 
class ActuatorDataListener(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        
    def onActuatorMessage(self, ActuatorData):
        redis = StrictRedis(host = 'localhost', port = 6379 , db = 0,)
        ps = redis.pubsub()
        ps.subscribe('Actuator')
        for item in ps.listen():
            if item['type'] == 'message':
                logging.info(item['data'])
        ps.close()        