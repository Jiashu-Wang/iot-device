'''
Created on 2020年2月19日

@author: Jiashu Wang
'''
from labs.common.SensorData import SensorData
from redis import StrictRedis
import logging 
class SensorDataListener(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        
    def onSensorMessage(self,SensorData):
        redis = StrictRedis(host = 'localhost', port = 6379 , db = 0,)
        ps = redis.pubsub()
        ps.subscribe('Sensor')
        for item in ps.listen():
            if item['type'] == 'message':
                logging.info("onMessage: " + str(item['data']))
        ps.close()