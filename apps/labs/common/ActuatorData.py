'''
Created on Feb 7, 2020

@author: Jiashu Wang
'''
from _datetime import datetime

class ActuatorData(object):
    '''
    classdocs
    '''
    timeStamp = None
    name = 'Not set'
    command = 0
    statusCode = 0
    msg = None
    newVal = 0.0

    def __init__(self):
        '''
        Constructor
        '''
        
    def getCommand(self):
        return self.command
    
    def getValue(self):
        return self.newVal
    
    def getName(self):
        return self.name
    
    def getmsg(self):
        return self.msg
    
    def getStatusCode(self):
        return self.statusCode
    
    def setCommand(self, command):
        self.command = command
    
    def setValue(self, newVal):
        self.newVal = newVal
    
    def setName(self, name):
        self.name = name
        
    def setmsg(self, msg):
        self.msg = msg
    
    def setStatusCode(self, statusCode):
        self.statusCode = statusCode
    
    '''
    add new value to actuator, update the message depends on command.
    '''
    def addValue(self, newCommand):
        self.timeStamp = str(datetime.now())
        
        self.command = newCommand
        
        
        if(self.command == 0):
            self.msg = "OK!"
        elif (self.command == 1):
            self.msg = "Cold! " + str(self.statusCode) + " degree."
        elif (self.command == 2):
            self.msg = "Hot!" + str(self.statusCode) + " degree."
        else:
            print("Actuator Error.")
            print(self.command)
    
    def __str__(self):
        sensorStr = str(self.name) + ':\n' + '        Time:        ' + str(self.timeStamp) + ' \n        Current:   ' + str(self.curValue) + '\n        Average:  ' + str(self.avgValue) + "\n        Samples:  " + str(self.sampleCount)  + '\n        Min:       ' + str(self.minValue) +'\n        Max:      ' + str(self.maxValue)
        return sensorStr
        