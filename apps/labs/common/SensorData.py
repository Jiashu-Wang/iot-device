'''
Created on Jan 29, 2020

@author: Jiashu Wang
'''
from _datetime import datetime

class SensorData(object):
    '''
    classdocs
    '''
    timeStamp = None
    name = 'Not set'
    curValue = float(0)
    avgValue = float(0)
    minValue = float(0)
    maxValue = float(0)
    totValue = float(0)
    sampleCount = 0

    def __init__(self):
        '''
        Constructor
        '''
        '''
        add current value, change total value
        '''
    def addValue(self, newVal):
        self.sampleCount += 1
        self.timeStamp = str(datetime.now())
        self.curValue = newVal
        self.totValue += newVal
        if (self.curValue < self.minValue):
            self.minValue = self.curValue
        if (self.curValue > self.maxValue):
            self.maxValue = self.curValue
        if (self.totValue != 0 and self.sampleCount > 0):
            self.avgValue = self.totValue / self.sampleCount
    
    def getAverageValue(self):
        return self.avgValue

    def getCount(self):
        return self.sampleCount
    
    def getCurrentValue(self):
        return self.curValue
    
    def getMaxValue(self):
        return self.maxValue
    
    def getMinValue(self):
        return self.minValue
    
    def getName(self):
        return self.name
    
    def setName(self, newName):
        self.name = newName
        '''
        format the output of sensor data
        '''
    def __str__(self):
        sensorStr = str(self.name) + ':\n' + '        Time:        ' + str(self.timeStamp) + ' \n        Current:   ' + str(self.curValue) + '\n        Average:  ' + str(self.avgValue) + "\n        Samples:  " + str(self.sampleCount)  + '\n        Min:       ' + str(self.minValue) +'\n        Max:      ' + str(self.maxValue)
        return sensorStr