'''
Created on 2020年2月15日

@author: Jiashu Wang
'''
from labs.common.ActuatorData import ActuatorData
import logging
from labs.module04.SenseHatLedActivator import SenseHatLedActivator
class MultiActuatorAdaptor(object):
    '''
    update actuator and show message on senseHat LED matrix
    '''
    actuator = None
    logging.basicConfig(level = logging.INFO, format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    logger = logging.getLogger(__name__)


    def __init__(self, params):
        '''
        Constructor
        '''
        self.actuator = ActuatorData()
    
    def updateActuator(self,actuator):
        isSuccess = False

        self.actuator = actuator
        senseLed = SenseHatLedActivator()
        isSuccess = senseLed.showMessageOnLed(self.actuator.getCommand(), str(self.actuator.getmsg()))
        
        if isSuccess == True:           
            self.logger.info("Show Message On Led successfully")
        else:
            self.logger.info("Shoe message error.")
        return isSuccess