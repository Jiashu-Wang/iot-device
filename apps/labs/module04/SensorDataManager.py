'''
Created on Feb 7, 2020

@author: Jiashu Wang
'''
from labs.common.ActuatorData import ActuatorData
from labs.common.SensorData import SensorData
from labs.common.ConfigUtil import ConfigUtil
from labs.module02.SmtpClientConnector import SmtpClientConnector
from labs.module04.MultiActuatorAdaptor import MultiActuatorAdaptor
from labs.module04.MultiSensorAdaptor import MultiSensorAdaptor
from labs.common.ConfigConst import ConfigConst
class SensorDataManager(object):
    '''
    classdocs
    '''
    sensorData = SensorData()
    actuatorData = ActuatorData()
    curTemp = 0
    threshold = 5
    connector = SmtpClientConnector()
    maa = MultiActuatorAdaptor()
    
    # to be complete
    nominalHumi = 20
    def __init__(self):
        '''
        Constructor
        '''
        self.config = ConfigUtil('D:/NEU/6530/workspace/iot-device/config/ConnectedDevicesConfig.props')
    def getSensorData(self):        
        msa = MultiSensorAdaptor()
        msa.updateSensorData(self)        
        
    '''
    this method compare the current sensor data with nominal
    the result dicides the actuator command
    and send email using SMTPconnector
    ''' 
    def handleSensorData(self, SensorData):        
        self.sensorData = SensorData
        if(abs(self.sensorData.getCurrentValue() - self.nominalHumi) <= self.threshold):
            self.actuatorData.addValue(0, self.sensorData.getCurrentValue())
            self.connector.publishMessage("Humidity is stable", self.sensorData)
        if(self.sensorData.getCurrentValue() - self.nominalHumi < (0 - self.threshold)):
            self.actuatorData.addValue(1, self.sensorData.getCurrentValue())
            self.connector.publishMessage("Humidity is too low", self.sensorData)
        if(self.sensorData.getCurrentValue() - self.nominalHumi > self.threshold):
            self.actuatorData.addValue(2, self.sensorData.getCurrentValue())
            self.connector.publishMessage("Humidity is too high", self.sensorData)
        self.maa.updateActuator(self.actuatorData)
        return self.actuatorData
        