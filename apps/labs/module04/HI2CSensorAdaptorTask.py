'''
Created on 2020年2月15日

@author: Jiashu Wang
'''

import threading
from time import sleep
from sense_hat import SenseHat
from labs.common.SensorData import SensorData
from labs.common import ConfigUtil
from labs.common import ConfigConst
from smbus2 import SMBus
import logging
i2cBus = SMBus(1) # Use I2C bus No.1 on Raspberry Pi 3+
enableControl = 0x2D
enableMeasure = 0x08
accelAddr = 0x1C # address for IMU (accelerometer)
magAddr = 0x6A # address for IMU (magnetometer)
pressAddr = 0x5C # address for pressure sensor
humidAddr = 0x5F # address for humidity sensor
begAddr = 0x28
totBytes = 6
DEFAULT_RATE_IN_SEC = 10

class HI2CSensorAdaptorTask(threading.Thread):
    '''
    read data from I2Cbus and calculate humidity 
    '''
    sensorData = SensorData()
    currentHum = 0
    maxVal = 30
    minVal = 0
    intervalTime = 5
    senseHat = SenseHat
    rateInSec = DEFAULT_RATE_IN_SEC

    def __init__(self, params):
        '''
        Constructor
        '''
        super(HI2CSensorAdaptorTask, self).__init__()
        self.config = ConfigUtil.ConfigUtil(ConfigConst.ConfigConst.DEFAULT_CONFIG_FILE_NAME)
        self.config.loadConfig()
        print('Configuration data...\n' + str(self.config))
        self.initI2CBus()
        
    def initI2CBus(self):
        logging.info("Initializing I2C bus and enabling I2C addresses...")
        i2cBus.write_byte_data(accelAddr, 0, 0)
        i2cBus.write_byte_data(magAddr, 0, 0)
        i2cBus.write_byte_data(pressAddr, 0, 0)
        i2cBus.write_byte_data(humidAddr, 0, 0)
    
    '''
        read the data in registers (HTS 221 sensor technical note) and return the humidity
    '''
    def displayHumidityData(self):
        H_OUT_LSB = i2cBus.read_byte_data(0x5f, 0x28)
        H_OUT_MSB = i2cBus.read_byte_data(0x5f, 0x29)
        H_OUT = self.data_convert(H_OUT_LSB, H_OUT_MSB)
        H0_T0_OUT_LSB = i2cBus.read_byte_data(0x5f, 0x36)
        H0_T0_OUT_MSB = i2cBus.read_byte_data(0x5f, 0x37)
        H0_T0_OUT = self.data_convert(H0_T0_OUT_LSB, H0_T0_OUT_MSB)
        H1_T0_OUT_LSB = i2cBus.read_byte_data(0x5f, 0x3A)
        H1_T0_OUT_MSB = i2cBus.read_byte_data(0x5f, 0x3B)
        H1_T0_OUT = self.data_convert(H1_T0_OUT_LSB, H1_T0_OUT_MSB)
        
        H0_rH = i2cBus.read_byte_data(0x5f, 0x30)>>1
        H1_rH = i2cBus.read_byte_data(0x5f, 0x31)>>1
        
        humidityData = (H1_rH - H0_rH)*(H_OUT - H0_T0_OUT) / (H1_T0_OUT - H0_T0_OUT) +H0_rH
        
        return humidityData
        
    def data_convert(self, lsb, msb):
        """
        Convert lsb and msb data to int_16
        @param [in] data1 LSB
        @param [in] data2 MSB
        @return Value MSB+LSB(int 16bit)
        """
        value = (msb << 8) | lsb
        if value & (1 << 16 - 1):
            value -= (1 << 16)
        return value
    
    def getCurrentHum(self):
        return self.currentHum
    
    def run(self):
        while True:
            self.senseHat = SenseHat()
            #get humidity from I2Cbus
            self.currentHum = self.displayHumidityData()
            self.sensorData.addValue(self.currentHum)
            self.sensorData.setName("HI2C Humidity")    
            logging.info("Humidity from I2C: " + str(self.currentHum))
            I2CHumi = self.currentHum
            sleep(self.rateInSec)
            return I2CHumi
    
    
            