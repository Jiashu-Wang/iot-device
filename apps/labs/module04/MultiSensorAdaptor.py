'''
Created on 2020年2月15日

@author: Jiashu Wang
'''
from labs.module04.HumiditySensorAdaptorTask import HumiditySensorAdaptorTask
from labs.module04.HI2CSensorAdaptorTask import HI2CSensorAdaptorTask
import logging
from time import sleep
class MultiSensorAdaptor(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
    
    '''
    start the thread to get data from senseHat,
    calculate the error range between API and I2C humidity data
    '''
    def updateSensorData(self):
        hs = HumiditySensorAdaptorTask()
        hI2CS = HI2CSensorAdaptorTask()
       
        hs.start()
        hI2CS.start()
        
        i = 0
        while i < 10:
            sleep(10)
            APIdata = hs.getCurrentHum()            
            I2Cdata = hI2CS.getCurrentHum()
            errorRange = (APIdata - I2Cdata) / APIdata * 100
            logging.info("error is: " + str(errorRange) + "%")
            i += 1