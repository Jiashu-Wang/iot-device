'''
Created on 2020年2月15日

@author: Jiashu Wang
'''
from labs.common.SensorData import SensorData
# import labs.module04.SensorDataManager
from sense_hat import SenseHat
from time import sleep
import threading
import logging

class HumiditySensorAdaptorTask(threading.Thread):
    '''
    To get humidity data from senseHat API
    '''
    sensorData = SensorData()
    currentHum = 0
    maxVal = 30
    minVal = 0
    intervalTime = 30
    senseHat = SenseHat

    def __init__(self):
        '''
        Constructor
        '''
        threading.Thread.__init__(self)
    
    def getCurrentHum(self):
        return self.currentHum    
    def run(self):
        while True:
            self.senseHat = SenseHat()
            # use get_humidity to read data
            self.currentHum = self.senseHat.get_humidity()
            self.sensorData.addValue(self.currentHum)
            self.sensorData.setName("APIHumidity")
#             SDM = labs.module04.SensorDataManager.SensorDataManager()
#             SDM.handleSensorData(self.sensorData)
            logging.info("Humidity from  API" + str(self.currentHum))
            sleep(self.intervalTime)
            