'''
Created on Feb 8, 2020

@author: Jiashu Wang
'''
from sense_hat import SenseHat

class SenseHatLedActivator():
    '''
    classdocs
    '''
    command = 0
    msg = None
    
    def __init__(self):
        '''
        Constructor
        '''
    '''
    this method invokes show_message in senseHat package to show message on 
    Led. Because I use dummy sense_hat, it can not decide the color of the message.
    command 0 --> color = white
    command 1 --> color = blue
    command 2 --> color = red
    '''
    def showMessageOnLed(self,command, msg):
        sense = SenseHat()
        if(command == 0):
            sense.show_message(msg, text_colour=[255, 255, 255])
            return True
        if(command == 1):
            sense.show_message(msg, text_colour=[0, 0, 255])
            return True
        if(command == 2):
            sense.show_message(msg, text_colour=[255, 0, 0])
            return True
        return False    