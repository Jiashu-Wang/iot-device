'''
Created on Feb 7, 2020

@author: Jiashu Wang
'''
from labs.module03.TempSensorAdaptorTask import TempSensorAdaptorTask


class TempSensorAdaptor(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
    '''
    start the thread to get data from senseHat
    '''
    def updateSensorData(self):
        ts = TempSensorAdaptorTask()
        
        ts.start()
        return ts.sensorData
        