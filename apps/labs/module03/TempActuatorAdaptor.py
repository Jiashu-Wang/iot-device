'''
Created on Feb 7, 2020

@author: Jiashu Wang
'''
from labs.module03.SenseHatLedActivator import SenseHatLedActivator
from labs.common.ActuatorData import ActuatorData
import logging
class TempActuatorAdaptor(object):
    '''
    classdocs
    '''
    actuator = None
    logging.basicConfig(level = logging.INFO, format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    logger = logging.getLogger(__name__)

    def __init__(self):
        '''
        Constructor
        '''
        self.actuator = ActuatorData()
       
        
        '''
        this method invokes showMessageOnLed to update actuator 
        '''
    
    def updateActuator(self,actuator):
        isSuccess = False
        
        
        
        self.actuator = actuator
        senseLed = SenseHatLedActivator()
        isSuccess = senseLed.showMessageOnLed(self.actuator.getCommand(), str(self.actuator.getmsg()))
        
        if isSuccess == True:           
            self.logger.info("Show Message On Led successfully")
        else:
            self.logger.info("Shoe message error.")
        return isSuccess