'''
Created on Feb 7, 2020

@author: Jiashu Wang
'''
from labs.common.ActuatorData import ActuatorData
from labs.common.SensorData import SensorData
from labs.common.ConfigUtil import ConfigUtil
from labs.module02.SmtpClientConnector import SmtpClientConnector
from labs.module03.TempActuatorAdaptor import TempActuatorAdaptor
from labs.module03.TempSensorAdaptor import TempSensorAdaptor
from labs.common.ConfigConst import ConfigConst
class SensorDataManager(object):
    '''
    classdocs
    '''
    sensorData = SensorData()
    actuatorData = ActuatorData()
    curTemp = 0
    threshold = 5
    connector = SmtpClientConnector()
    taa = TempActuatorAdaptor()
    
    # to be complete
    nominalTemp = 0
    def __init__(self):
        '''
        Constructor
        '''
        self.config = ConfigUtil('D:/NEU/6530/workspace/iot-device/config/ConnectedDevicesConfig.props')
    def getSensorData(self):        
        tsa = TempSensorAdaptor
        self.sensorData = tsa.updateSensorData(self)        
        
    '''
    this method compare the current sensor data with nominal
    the result dicides the actuator command
    and send email using SMTPconnector
    ''' 
    def handleSensorData(self, SensorData):
        self.nominalTemp = float(self.config.getValue(ConfigConst.CONSTRAINED_DEVICE, ConfigConst.TEMP_KEY))
        self.sensorData = SensorData
        if(abs(self.sensorData.getCurrentValue() - self.nominalTemp) <= self.threshold):
            self.actuatorData.addValue(0)
            self.connector.publishMessage("Temperature is stable", self.sensorData)
        if(self.sensorData.getCurrentValue() - self.nominalTemp < (0 - self.threshold)):
            self.actuatorData.addValue(1)
            self.connector.publishMessage("Temperature is too low", self.sensorData)
        if(self.sensorData.getCurrentValue() - self.nominalTemp > self.threshold):
            self.actuatorData.addValue(2)
            self.connector.publishMessage("Temperature is too high", self.sensorData)
        self.taa.updateActuator(self.actuatorData)
        return self.actuatorData
        