'''
Created on Feb 7, 2020

@author: Jiashu Wang
'''
from threading import Thread
from labs.common.SensorData import SensorData
import labs.module03.SensorDataManager
from sense_hat import SenseHat
from time import sleep
import threading
class TempSensorAdaptorTask(threading.Thread):
    '''
    classdocs
    '''
    sensorData = SensorData()
    currentTemp = 0
    maxVal = 30
    minVal = 0
    intervalTime = 60
    senseHat = SenseHat
    def __init__(self):
        '''
        Constructor
        '''
        threading.Thread.__init__(self)
    
    '''
    get temperature from senseHat and return sensorData to sensordataManager
    '''
    def run(self,):
        while True:
            self.senseHat = SenseHat()
            self.currentTemp = self.senseHat.get_temperature()
            self.sensorData.addValue(self.currentTemp)
            self.sensorData.setName("Temperature")
            SDM = labs.module03.SensorDataManager.SensorDataManager()
            SDM.handleSensorData( self.sensorData)
            
            sleep(self.intervalTime)
  