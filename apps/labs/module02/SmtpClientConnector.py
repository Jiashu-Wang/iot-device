'''
Created on Jan 29, 2020

@author: Jiashu Wang
'''
from labs.common import ConfigUtil, ConfigConst
import logging
from email.mime.multipart import MIMEMultipart
import smtplib
from email.mime.text import MIMEText

class SmtpClientConnector(object):
    '''
    classdocs
    '''

    def __init__(self):
        '''
        Constructor
        '''
        self.config = ConfigUtil.ConfigUtil('D:/NEU/6530/workspace/iot-device/config/ConnectedDevicesConfig.props')
        #self.config.loadConfig()
        
        logging.info('Configguration data...\n' + str(self.config))
    
    def publishMessage(self, topic, data):
        isSuccess = False
        try:
            host        = self.config.getValue(ConfigConst.ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.ConfigConst.HOST_KEY) 
            port        = self.config.getValue(ConfigConst.ConfigConst.SMTP_CLOUD_SECTION,ConfigConst.ConfigConst.PORT_KEY)
            fromAddr    = self.config.getValue(ConfigConst.ConfigConst.SMTP_CLOUD_SECTION,ConfigConst.ConfigConst.FROM_ADDRESS_KEY)
            toAddr      = self.config.getValue(ConfigConst.ConfigConst.SMTP_CLOUD_SECTION,ConfigConst.ConfigConst.TO_ADDRESS_KEY)
            authToken   = self.config.getValue(ConfigConst.ConfigConst.SMTP_CLOUD_SECTION,ConfigConst.ConfigConst.USER_AUTH_TOKEN_KEY)
            # setup mail msg information
            msg         = MIMEMultipart()
            msg['From'] = fromAddr
            msg['TO']   = toAddr
            msg['Subject'] = topic 
            msgBody     = str(data)
            
            msg.attach(MIMEText(msgBody))
            
            msgText     = msg.as_string()
            #make connection and send mail, close the connection after all
            smtpServer = smtplib.SMTP_SSL(host, port)
            smtpServer.ehlo()
            smtpServer.login(fromAddr,authToken)
            smtpServer.sendmail(fromAddr, toAddr, msgText)
            smtpServer.close()
            isSuccess = True
        except IOError:
            return isSuccess
        return isSuccess