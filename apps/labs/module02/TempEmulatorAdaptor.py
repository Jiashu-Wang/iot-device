'''
Created on Jan 29, 2020

@author: Jiashu Wang
'''
from labs.module02.TempSensorEmulatorTask import TempSensorEmulatorTask
from time import sleep

if __name__ == '__main__':
    ts = TempSensorEmulatorTask()
    ts.daemon = True
    
    print('Deamon Thread starting')
    ts.start()
    
    while True:
        sleep(5)
        pass