'''
Created on Jan 29, 2020

@author: Jiashu Wang
'''
from labs.common import SensorData
import threading
from threading import Thread
from labs.module02.SmtpClientConnector import SmtpClientConnector
from random import uniform
from time import sleep
class TempSensorEmulatorTask(threading.Thread):
    '''
    classdocs
    '''
    sensorData = SensorData.SensorData()
    currentTemp = 0
    maxVal = 30
    minVal = 0
    intervalTime = 20
    threshold = 5
    smtpConnector = SmtpClientConnector()

    def __init__(self):
        '''
        Constructor
        '''
        Thread.__init__(self)
        
    '''
     initialize senor data and add current temp
    '''   
    def getSensorData(self):
        self.sensorData.setName('Temperature')
        self.currentTemp = uniform(float(self.minVal), float(self.maxVal))
        self.sensorData.addValue(self.currentTemp)
        
    def run(self):
        while True:
            self.getSensorData()
            if abs(self.currentTemp - self.sensorData.getAverageValue()) > self.threshold :
                print('\n CurrTemp is' + str(self.currentTemp))
                # send email to mail account
                self.smtpConnector.publishMessage('Temperature warning', self.sensorData)
            sleep(self.intervalTime)