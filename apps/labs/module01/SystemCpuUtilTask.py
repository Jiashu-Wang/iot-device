'''
Created on Jan 25, 2020

@author: Jiashu Wang
'''
import psutil
import logging

class SystemCpuUtilTask:
#return a float value representing the on-demand CPU utilization
    
    def getDataFromSensor():
        CpuPercent = psutil.cpu_percent(None, False)
        cpuUtil = str(CpuPercent)
        logging.basicConfig(level = logging.INFO, filename = 'SystemUtil.log', filemode = 'a',format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        logger = logging.getLogger(__name__)
        logger.info("CPU Utilization = " + cpuUtil)
        return CpuPercent