'''
Created on 2020��1��22��

@author: Jiashu Wang
'''


from labs.module01 import SystemCpuUtilTask, SystemMemUtilTask

class SystemPerformanceAdaptor:
#displaying the output from each Task class
    def showPerformance():
        CpuUtil = SystemCpuUtilTask.SystemCpuUtilTask.getDataFromSensor()

        MemUtil = SystemMemUtilTask.SystemMemUtilTask.getDataFromSensor()
