'''
Created on Jan 25, 2020

@author: Jiashu Wang
'''
from labs.module01 import SystemPerformanceAdaptor
import time 
'''
 getting the CPU utilization percentage and virtual (or real) memory
         utilization percentage by 10 times
'''
if __name__ == '__main__':
    for i in range(0,10):
        sysPerformance = SystemPerformanceAdaptor.SystemPerformanceAdaptor.showPerformance();        
        
        time.sleep(2)