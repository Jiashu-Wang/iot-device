'''
Created on Jan 25, 2020

@author: Jiashu Wang
'''
import psutil
import logging

class SystemMemUtilTask:
#return a float value representing the on-demand memory utilization
    def getDataFromSensor():
        MemPercent = psutil.virtual_memory().percent
        MemUtil = str(MemPercent)
        logging.basicConfig(level = logging.INFO, filename = 'MemoryUtil.log', filemode = 'a',  format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        logger = logging.getLogger(__name__)
        logger.info("Memory Utilization = " + MemUtil)
        return MemPercent