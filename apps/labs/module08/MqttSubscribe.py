'''
Created on 2020年4月18日

@author: Jiashu Wang
'''
'''
This Example sends harcoded data to Ubidots using the Paho MQTT
library.

Please install the library using pip install paho-mqtt

Made by Jose García @https://github.com/jotathebest/
Adapted from the original Paho Subscribe example at
https://github.com/eclipse/paho.mqtt.python/blob/master/examples/client_sub.py
'''

import paho.mqtt.client as mqtt


BROKER_ENDPOINT = "things.ubidots.com"
PORT = 1883
MQTT_USERNAME = "BBFF-fxFo9o01O7DpeoHvn07pRyOFQo6T8D"  # Put here your TOKEN
MQTT_PASSWORD = ""
TOPIC = "/v1.6/devices"
DEVICE_LABEL = "pi-iot"
VARIABLE_LABEL = "tempactuator"


def on_connect(mqttc, obj, flags, rc):
    print("[INFO] Connected!")
    topic = "{}/{}/{}/lv".format(TOPIC, DEVICE_LABEL, VARIABLE_LABEL)
    print(topic)
    mqttc.subscribe(topic, 0)

def on_message(mqttc, obj, msg):
    print("[INFO] value received: {}".format(float(msg.payload)))


def on_publish(mqttc, obj, mid):
    print("[INFO] Published!")


def on_subscribe(mqttc, obj, mid, granted_qos):
    print("[INFO] Subscribed!")


def on_log(mqttc, obj, level, string):
    print("[INFO] Log info: {}".format(string))

def main():
    # Setup MQTT client
    mqttc = mqtt.Client()
    mqttc.username_pw_set(MQTT_USERNAME, password="")
    mqttc.on_message = on_message
    mqttc.on_connect = on_connect
    mqttc.on_publish = on_publish
    mqttc.on_subscribe = on_subscribe

    # Uncomment to enable debug messages
    # mqttc.on_log = on_log

    mqttc.connect(BROKER_ENDPOINT, PORT, 60)
    topic = "{}/{}/{}/lv".format(TOPIC, DEVICE_LABEL, VARIABLE_LABEL)
    print(topic)
    mqttc.subscribe(topic, 0)

    mqttc.loop_forever()

if __name__ == '__main__':
    main()