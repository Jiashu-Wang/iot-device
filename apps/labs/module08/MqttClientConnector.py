'''
Created on 2020年3月31日

@author: Jiashu Wang
'''
from labs.common.ActuatorData import ActuatorData
from labs.common.SensorData import SensorData
import paho.mqtt.client as mqttClient
import time
import json
import ssl
import logging
from labs.module08 import MqttTLSTest

'''
global variables
'''

connected = False  # Stores the connection status
BROKER_ENDPOINT = "things.ubidots.com"
TLS_PORT = 8883  # Secure port
MQTT_USERNAME = "BBFF-fxFo9o01O7DpeoHvn07pRyOFQo6T8D"  # Put here your Ubidots TOKEN
MQTT_PASSWORD = ""  # Leave this in blank
TOPIC = "/v1.6/devices/"
DEVICE_LABEL = "iot/"
VARIABLE_LABEL = "TempSensor"
VARIABLE_LABEL_ACT = "TempActuator"
TLS_CERT_PATH = "D:\\NEU\\6530\\workspace\\iot-device\\ubidots_cert.pem"  # Put here the path of your TLS cert


def on_connect(mqttc, obj, flags, rc):
    print("rc: " + str(rc))
    global connected
    connected = True


def on_message(mqttc, obj, msg):
    logging.info("Get subscribed message...")
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))


def on_publish(mqttc, obj, mid):
    print("mid: " + str(mid))
    pass


def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))
    

# def on_message(client, userdata, msg):
#     print(msg.topic + " " + ": " + str(msg.payload))

def connect(mqtt_client, mqtt_username, mqtt_password, broker_endpoint, port):
    global connected
    mqtt_client.on_message = on_message
    if not connected:
        mqtt_client.username_pw_set(mqtt_username, password=mqtt_password)
        mqtt_client.on_connect = on_connect
        mqtt_client.on_publish = on_publish
        mqtt_client.on_subscribe = on_subscribe
        
        mqtt_client.tls_set(ca_certs=TLS_CERT_PATH, certfile=None,
                            keyfile=None, cert_reqs=ssl.CERT_REQUIRED,
                            tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)
        mqtt_client.tls_insecure_set(False)
        mqtt_client.connect(broker_endpoint, port=port)
        mqtt_client.loop_start()

        attempts = 0

        while not connected and attempts < 5:  # Wait for connection
            print(connected)
            print("Attempting to connect...")
            time.sleep(1)
            attempts += 1

    if not connected:
        print("[ERROR] Could not connect to broker")
        return False

    return True


def publish(mqtt_client, topic, payload):

    try:
        mqtt_client.publish(topic, payload)

    except Exception as e:
        print("[ERROR] Could not publish data, error: {}".format(e))

def subscirbe(mqtt_client, topic, qos):
    try:
        mqtt_client.subscribe(topic, qos)
        logging.info("subscribing...")

        mqtt_client.on_subscribe = on_subscribe
        mqtt_client.on_message = on_message
    except Exception as e:
        print("[ERROR] Could not subscribe data, error: {}".format(e))
            
class MqttClientConnector(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        useTLS = False
        rootCertPath = None
        
    
    
                    
    def publishSensorData(self, SensorData, qos):
        payload = json.dumps({"value": SensorData.curValue})
        topic = "{}{}{}".format(TOPIC, DEVICE_LABEL, VARIABLE_LABEL)
        logging.info(payload)
        logging.info(topic)
        mqtt_client = mqttClient.Client()
        if not connect(mqtt_client, MQTT_USERNAME, MQTT_PASSWORD, BROKER_ENDPOINT, TLS_PORT):
            return False
        
        publish(mqtt_client, topic, payload)
        mqtt_client.disconnect()
        global connected
        connected = False
        return True
    def publishActuatorCommand(self, ActuatorData, qos):
        payload = json.dumps({"value": ActuatorData.command})
        topic = "{}{}{}".format(TOPIC, DEVICE_LABEL, VARIABLE_LABEL_ACT)
        mqtt_client = mqttClient.Client()
        if not connect(mqtt_client, MQTT_USERNAME, MQTT_PASSWORD, BROKER_ENDPOINT, TLS_PORT):
            return False
        
        publish(mqtt_client, topic, payload)
        mqtt_client.disconnect()
        global connected
        connected = False
        return True
    
    def subscribeActuatorData(self, qos):
        topic = "{}{}{}".format(TOPIC, DEVICE_LABEL, VARIABLE_LABEL_ACT)
        logging.info(topic)
        mqtt_client = mqttClient.Client()
        
        if not connect(mqtt_client, MQTT_USERNAME, MQTT_PASSWORD, BROKER_ENDPOINT, TLS_PORT):
            return False
        subscirbe(mqtt_client, topic, qos)
        mqtt_client.loop_forever()
       # mqtt_client.disconnect()
        global connected
        connected = False
        return True
