'''
Created on 2020年3月31日

@author: Jiashu Wang
'''
from labs.common.SensorData import SensorData
from labs.common.ActuatorData import ActuatorData
from labs.module08.UbidotsClientConnector import UbidotsClientConnector
import logging
from sense_hat import SenseHat
import time
logging.basicConfig(level = logging.INFO,filename = 'UbidotsConnection.log', filemode = 'a', format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

if __name__ == '__main__':
#     sensorDataAPI = SensorData()
#     sensorDataAPI.addValue(26)
    
#     ubidots_client.publishSensorDataUsingAPI(sensorDataAPI)
    ubidots_client = UbidotsClientConnector()
    sensorDataMqtt = SensorData()
    sense_Hat = SenseHat()
#    ubidots_client.subscribeToTopic(1)
    actuatorDataMqtt = ActuatorData()
    while True:
        sensorDataMqtt.addValue(sense_Hat.get_temperature())
        ubidots_client.publishSensorDataUsingMqtt(sensorDataMqtt)
        
        actuatorDataMqtt.addValue(1, 1)
        ubidots_client.publishActuatorDataUsingMqtt(actuatorDataMqtt)
        time.sleep(10)
    pass