import unittest
from labs import common

"""
Test class for all requisite SensorData functionality.

Instructions:
1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
2) Add the '@Test' annotation to each new 'test...()' method you add.
3) Import the relevant modules and classes to support your tests.
4) Run this class as unit test app.
5) Include a screen shot of the report when you submit your assignment.

Please note: While some example test cases may be provided, you must write your own for the class.
"""
from labs.common.SensorData import SensorData
from random import uniform

class SensorDataTest(unittest.TestCase):

	"""
	Use this to setup your tests. This is where you may want to load configuration
	information (if needed), initialize class-scoped variables, create class-scoped
	instances of complex objects, initialize any requisite connections, etc.
	"""
	def setUp(self):
		self.sensorData = SensorData()
		pass

	"""
	Use this to tear down any allocated resources after your tests are complete. This
	is where you may want to release connections, zero out any long-term data, etc.
	"""
	def tearDown(self):
		pass
	
	"""
	test whether add value correctly
	"""
	def testaddValue(self):
		self.sensorData.addValue(float(20))
		self.assertEqual(20, self.sensorData.curValue)
		self.assertIsInstance(self.sensorData.curValue, float)
		pass
	'''
	test temp values' type are  float
	'''
	def testisFloat(self):
		self.assertIsInstance(self.sensorData.curValue, float)
		self.assertIsInstance(self.sensorData.avgValue, float)
		self.assertIsInstance(self.sensorData.minValue, float)
		self.assertIsInstance(self.sensorData.maxValue, float)
		self.assertIsInstance(self.sensorData.totValue, float)

if __name__ == "__main__":
	#import sys;sys.argv = ['', 'Test.testName']
	unittest.main()