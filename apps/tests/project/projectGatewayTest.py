'''
Created on 2020年4月18日

@author: Jiashu Wang
'''
import unittest

from labs.common.SensorData import SensorData
from labs.module08.UbidotsClientConnector import UbidotsClientConnector
import logging
from sense_hat import SenseHat
class Test(unittest.TestCase):


    def setUp(self):
        pass


    def tearDown(self):
        pass


    def testpublishSensorDataUsingMqtt(self):
        ubidots_client = UbidotsClientConnector()
        sensorDataMqtt = SensorData()
        sense_Hat = SenseHat()
        sensorDataMqtt.addValue(sense_Hat.get_temperature())
        ubidots_client.publishSensorDataUsingMqtt(sensorDataMqtt)
        pass
    def testpublishSensorDataUsingAPI(self):
        ubidots_client = UbidotsClientConnector()
        sensorData = SensorData()
        sense_Hat = SenseHat()
        sensorData.addValue(sense_Hat.get_temperature())
        ubidots_client.publishTempDataUsingAPI(sensorData)
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()