'''
Created on Jan 25, 2020

@author: Jiashu Wang
'''
import unittest
from labs.module01 import SystemCpuUtilTask


class Test(unittest.TestCase):


    def setUp(self):
        pass


    def tearDown(self):
        pass

    """
    the cpu percentage should be >=0 and <=100
    """
    def testCPU(self):
        self.assertTrue((0 <= SystemCpuUtilTask.SystemCpuUtilTask.getDataFromSensor() <= 100))
        self.assertFalse((SystemCpuUtilTask.SystemCpuUtilTask.getDataFromSensor() < 0))
        self.assertFalse((SystemCpuUtilTask.SystemCpuUtilTask.getDataFromSensor() > 100))


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testCPU']
    unittest.main()