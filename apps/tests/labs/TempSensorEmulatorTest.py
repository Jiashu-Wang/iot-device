'''
Created on Feb 1, 2020

@author: Jiashu Wang
'''
import unittest

from labs.module02.TempSensorEmulatorTask import TempSensorEmulatorTask


class Test(unittest.TestCase):


    def setUp(self):
        self.tempSensor = TempSensorEmulatorTask()
        pass


    def tearDown(self):
        pass

    '''
    test if the data is type of float, between min and max value
    '''
    def testgetSensorData(self):
        self.tempSensor.getSensorData()
        self.assertIsInstance(self.tempSensor.currentTemp, float)
        self.assertTrue(0 < self.tempSensor.currentTemp)
        self.assertTrue(30 > self.tempSensor.currentTemp)
        self.assertFalse(0 > self.tempSensor.currentTemp)
        self.assertFalse(30 < self.tempSensor.currentTemp)
        pass


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()