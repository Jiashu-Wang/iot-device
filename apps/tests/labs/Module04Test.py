import unittest

from labs.module04 import HI2CSensorAdaptorTask
from labs.module04 import HumiditySensorAdaptorTask
"""
Test class for all requisite Module04 functionality.

Instructions:
1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
2) Add the '@Test' annotation to each new 'test...()' method you add.
3) Import the relevant modules and classes to support your tests.
4) Run this class as unit test app.
5) Include a screen shot of the report when you submit your assignment.

Please note: While some example test cases may be provided, you must write your own for the class.
"""
class Module04Test(unittest.TestCase):

	"""
	Use this to setup your tests. This is where you may want to load configuration
	information (if needed), initialize class-scoped variables, create class-scoped
	instances of complex objects, initialize any requisite connections, etc.
	"""
	def setUp(self):
		pass

	"""
	Use this to tear down any allocated resources after your tests are complete. This
	is where you may want to release connections, zero out any long-term data, etc.
	"""
	def tearDown(self):
		pass

	"""
	test whether get humidity from senseHat API correctly
	"""
	def testHumiditySensorAdaptorTask(self):
		hum  = HumiditySensorAdaptorTask.HumiditySensorAdaptorTask()
		hum.start()
		apihumi = hum.getCurrentHum()
		self.assertTrue(0 <= apihumi <= 100)
		self.assertFalse(0 > apihumi)
		self.assertFalse(100 < apihumi)
		pass
	"""
	test whether get humidity from I2CBus correctly
	"""	
	def testHI2CSensorAdaptorTask(self):
		HI2C = HI2CSensorAdaptorTask.HI2CSensorAdaptorTask()
		HI2C.start()
		I2Chumi = HI2C.getCurrentHum()
		self.assertTrue(0 <= I2Chumi <= 100)
		self.assertFalse(0 > I2Chumi)
		self.assertFalse(100 < I2Chumi)
		pass

if __name__ == "__main__":
	#import sys;sys.argv = ['', 'Test.testName']
	unittest.main()