'''
Created on Jan 25, 2020

@author: Jiashu Wang
'''
import unittest
from labs.module01 import SystemMemUtilTask


class Test(unittest.TestCase):


    def setUp(self):
        pass


    def tearDown(self):
        pass

    """
    the memory percentage should be >=0 and <=100
    """
    def testMemory(self):
        self.assertTrue((0 <= SystemMemUtilTask.SystemMemUtilTask.getDataFromSensor() <= 100))
        self.assertFalse((SystemMemUtilTask.SystemMemUtilTask.getDataFromSensor() < 0))
        self.assertFalse((SystemMemUtilTask.SystemMemUtilTask.getDataFromSensor() > 100))


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testMemory']
    unittest.main()