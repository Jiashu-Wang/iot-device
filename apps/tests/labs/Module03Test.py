import unittest
from labs.module03 import SenseHatLedActivator
from labs.common.ActuatorData import ActuatorData
from labs.common.SensorData import SensorData
from labs.module03.SensorDataManager import SensorDataManager
"""
Test class for all requisite Module03 functionality.

Instructions:
1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
2) Add the '@Test' annotation to each new 'test...()' method you add.
3) Import the relevant modules and classes to support your tests.
4) Run this class as unit test app.
5) Include a screen shot of the report when you submit your assignment.

Please note: While some example test cases may be provided, you must write your own for the class.
"""
class Module03Test(unittest.TestCase):

	"""
	Use this to setup your tests. This is where you may want to load configuration
	information (if needed), initialize class-scoped variables, create class-scoped
	instances of complex objects, initialize any requisite connections, etc.
	"""
	def setUp(self):
		self.sensorData = SensorData()
		self.actuatorData = ActuatorData()
		self.SDM = SensorDataManager()
		pass

	"""
	Use this to tear down any allocated resources after your tests are complete. This
	is where you may want to release connections, zero out any long-term data, etc.
	"""
	def tearDown(self):
		pass

	"""
	test whether ShowMessageOnled 
	"""
	def testShowMessageOnLed(self):
		SenseHatLedActivator.SenseHatLedActivator.showMessageOnLed(self, 0, "hello")
		pass
	'''
	test whether set actuator data correctly and send e-mail
	'''
	def testhandleSensorData(self):
		self.sensorData.addValue(30)
		self.actuatorData = self.SDM.handleSensorData(self.sensorData)
		self.assertEqual(2, self.actuatorData.getCommand())
		self.assertNotEqual(0, self.actuatorData.getCommand())
		self.assertNotEqual(1, self.actuatorData.getCommand())
if __name__ == "__main__":
	#import sys;sys.argv = ['', 'Test.testName']
	unittest.main()