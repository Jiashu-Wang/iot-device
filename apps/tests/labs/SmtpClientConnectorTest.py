'''
Created on Jan 31, 2020

@author: Jiashu Wang
'''
import unittest
from labs.module02.SmtpClientConnector import SmtpClientConnector


class Test(unittest.TestCase):


    def setUp(self):
        self.smtpTest = SmtpClientConnector()
        pass


    def tearDown(self):
        pass


    def testName(self):
        self.assertTrue(self.smtpTest.publishMessage('hello!', 'this is a test'))
#         self.assertFalse(self.smtpTest.publishMessage('hello!', 'this is a test'))    #change the from address to an invalid email 
#         self.assertFalse(self.smtpTest.publishMessage('hello!', 'this is a test'))    #disconnect the network
        pass


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()